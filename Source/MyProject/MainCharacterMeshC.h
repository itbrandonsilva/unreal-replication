// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"
#include "MainCharacterMeshC.generated.h"

/**
 * 
 */
UCLASS()
class AMainCharacterMeshC : public ACharacter
{
	GENERATED_UCLASS_BODY()

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) OVERRIDE;
	//virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) OVERRIDE;

	void MoveForward(float Value);

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = DefaultValues)
	bool isMovingForward;

	void setMovingForward(bool movingForward);

	UFUNCTION(reliable, server, WithValidation)
	void setMovingForwardServer(bool movingForward);
	
};
