// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "MyProject.h"
#include "MainCharacterMeshC.h"


AMainCharacterMeshC::AMainCharacterMeshC(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

void AMainCharacterMeshC::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAxis("Backward", this, &AMainCharacterMeshC::MoveForward);
	InputComponent->BindAxis("Forward", this, &AMainCharacterMeshC::MoveForward);
}

void AMainCharacterMeshC::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f)) {
		setMovingForward(true);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("WORKS"));
		// find out which way is forward
		FRotator Rotation = Controller->GetControlRotation();
		// Limit pitch when walking or falling
		if (CharacterMovement->IsMovingOnGround() || CharacterMovement->IsFalling())
		{
			Rotation.Pitch = 0.0f;
		}
		// add movement in that direction
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	} else {
		setMovingForward(false);
	}
}

void AMainCharacterMeshC::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	//Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// These used to only replicate if PlayerCameraManager->GetViewTargetPawn() != GetPawn()
	// But, since they also don't update unless that condition is true, these values won't change, thus won't send
	// This is a little less efficient, but fits into the new condition system well, and shouldn't really add much overhead
	//DOREPLIFETIME_CONDITION(APlayerController, TargetViewRotation, COND_OwnerOnly);
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMainCharacterMeshC, isMovingForward);
}

void AMainCharacterMeshC::setMovingForward(bool movingForward)
{
	isMovingForward = movingForward;

	if (Role < ROLE_Authority)
	{
		setMovingForwardServer(movingForward);
	}
}

void AMainCharacterMeshC::setMovingForwardServer_Implementation(bool movingForward)
{
	setMovingForward(movingForward);
}

bool AMainCharacterMeshC::setMovingForwardServer_Validate(bool movingForward)
{
	return true;
}