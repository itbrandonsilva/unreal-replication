// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "MyProject.h"

#include "MyProject.generated.inl"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyProject, "MyProject" );
